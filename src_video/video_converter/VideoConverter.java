package video_converter;

import java.io.File;

import it.sauronsoftware.jave.AudioAttributes;
import it.sauronsoftware.jave.Encoder;
import it.sauronsoftware.jave.EncoderProgressListener;
import it.sauronsoftware.jave.EncodingAttributes;
import it.sauronsoftware.jave.FFMPEGLocator;
import it.sauronsoftware.jave.MultimediaInfo;
import it.sauronsoftware.jave.VideoAttributes;
import it.sauronsoftware.jave.VideoSize;

public class VideoConverter implements EncoderProgressListener {

	public VideoConverter(String sourceFile, String targetFile) {
		try {
			//Encoder encoder = new Encoder(new FFMPEGLocatorImp());
			Encoder encoder = new Encoder();
			

			File source = new File(sourceFile);
			File target = new File(targetFile);
			
			
			// Setting audio attributes
			AudioAttributes audio = new AudioAttributes();
			audio.setCodec("mp3");
			audio.setBitRate(56);
			//audio.setChannels(2);
			audio.setSamplingRate(44100);
			VideoAttributes video = new VideoAttributes();
			//video.setCodec("flv");
			video.setBitRate(new Integer(44100));
			video.setFrameRate(new Integer(15));
			video.setSize(new VideoSize(400, 300));
			EncodingAttributes attrs = new EncodingAttributes();
			attrs.setFormat("flv");
			attrs.setAudioAttributes(audio);
			attrs.setVideoAttributes(video);
			
			// Encoding
			encoder.encode(source, target, attrs, this);
		} catch (Exception e) {
			System.err.println("Error: " + e.getMessage());
			e.printStackTrace();
		}	
	}
		
	
	public static void main(String[] args) {
		//String[] args2 = {"teste2.avi", "Test.flv"};
		//args = args2;
		if (args.length != 2) {
			System.out.println("Usage: java -jar VideoConverter.jar <avi input file> <flv file output>");
		}
		else {
			new VideoConverter(args[0], args[1]);
		}
	}

	@Override
	public void message(String arg0) {
	}

	@Override
	public void progress(int arg0) {
		System.out.println("Conversion " + (arg0 / 10.0) + "% done");
	}

	@Override
	public void sourceInfo(MultimediaInfo arg0) {
	}

}

class FFMPEGLocatorImp extends FFMPEGLocator {
	@Override
	protected String getFFMPEGExecutablePath() {
		String osname = System.getProperty("os.name");
		if (osname.toLowerCase().contains("mac")) {
			return new File("ffmpeg_mac").getAbsolutePath();
		}
		else if (osname.toLowerCase().contains("linux")) {
			return new File("ffmpeg_linux").getAbsolutePath();
		}
		else {
			return new File("ffmpeg_win").getAbsolutePath();
		}
		
	}
}
