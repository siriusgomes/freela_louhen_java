import java.io.*;

public class Solver 
{
	public static void main(String args[])
	{
		String salted_password;
		try
		{
			
			BufferedReader salted_file = new BufferedReader(new InputStreamReader(new FileInputStream("salted_pass_only.txt")));
			
			// loop
			while(true) {
				// read line from the salted_pass_only.txt file
				salted_password = salted_file.readLine();
				// it will be null if the file is over, or it will be empty if it reads a empty line.
				if (salted_password == null || salted_password.isEmpty()) {
					break;
				}
				
				String salt = salted_password.substring(0,2);
				String stored_password =  salted_password.substring(2);
		
				// step through all words in the dictionary		
				BufferedReader dict_br = new BufferedReader(new InputStreamReader(new FileInputStream("dictionary")));
				String word;
				String word_encrypted;
				
				boolean found = false;
							
				while ((word = dict_br.readLine()) != null && found == false)   
				{
					word_encrypted = JCrypt.crypt(salt, word);
	
					// Print the content on the console
					if (word_encrypted.equals((salt + stored_password).trim()))
					{
						found = true;
						System.out.println(word + " found!");
					}
				}
				dict_br.close();
    		}
			salted_file.close();
		}
		catch (Exception e)
		{
  			System.err.println("Error: " + e.getMessage());
		}
		
	}
}