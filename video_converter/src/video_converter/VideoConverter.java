package video_converter;

import java.io.File;

import it.sauronsoftware.jave.AudioAttributes;
import it.sauronsoftware.jave.Encoder;
import it.sauronsoftware.jave.EncoderProgressListener;
import it.sauronsoftware.jave.EncodingAttributes;
import it.sauronsoftware.jave.FFMPEGLocator;
import it.sauronsoftware.jave.MultimediaInfo;
import it.sauronsoftware.jave.VideoAttributes;

public class VideoConverter implements EncoderProgressListener {

	public VideoConverter(Encoder encoder, String sourceFile, String targetFile) {
		try {
			// Setting the file objects for the source and the target file.
			File source = new File(sourceFile);
			File target = new File(targetFile);
			
			
			// Setting audio attributes (such as bitrate, etc.)
			AudioAttributes audio = new AudioAttributes();
			// Setting video attributes (such as bitrate, etc.)
			VideoAttributes video = new VideoAttributes();
			
			EncodingAttributes attrs = new EncodingAttributes();
			attrs.setFormat("flv"); // Never remove
			attrs.setAudioAttributes(audio); // These properties are "null", so the result video will have the same properties as the source video
			attrs.setVideoAttributes(video); // These properties are "null", so the result video will have the same properties as the source video
			
			// encoder.encode will basically call the encode method from the library jave, which is embedded in this project.
			// this method will get the attributes and it will call the ffmpeg library that will perform the file conversion.
			encoder.encode(source, target, attrs, this);
		} catch (Exception e) {
			System.err.println("Error: " + e.getMessage());
			e.printStackTrace();
		}	
	}
		
	
	public static void main(String[] args) {
		//String[] args2 = {"test.avi", "Test.flv"};
		//args = args2;
		if (args.length != 2) {
			System.out.println("Usage: java -jar VideoConverter.jar <avi input file> <flv file output>");
		}
		else {
			
			//Grabing OS name from Java for testing which library of ffmpeg should be used.
			String osname = System.getProperty("os.name");
			String path = null;
			Encoder encoder;
			// In this case, it's mac, lets use mac version of ffmpeg!
			if (osname.toLowerCase().contains("mac")) {
				encoder = new Encoder(new FFMPEGLocatorImp());
			}
			// In this case, it's linux, lets use linux version of ffmpeg!
			else if (osname.toLowerCase().contains("linux")) {
				encoder = new Encoder(new FFMPEGLocatorImp());
			}
			// In this case, it's windows, lets use native ffmpeg! (Tests with the embedded one didn't work well)
			else { 
				encoder = new Encoder();
			}
			
			// Instanciate the new AudioConverter passing the paths as arguments.
			new VideoConverter(encoder, args[0], args[1]);
		}
	}

	@Override
	public void message(String arg0) {
	}

	// Implementing the EncoderProgressListener forces us to override the method progress. This method
	// will be called with an int between 0 and 1000 indicating the progress of the conversion. 
	// In this case, let's print out the progress to give some feedback to the user.
	@Override
	public void progress(int arg0) {
		System.out.println("Conversion " + (arg0 / 10.0) + "% done");
	}

	@Override
	public void sourceInfo(MultimediaInfo arg0) {
	}

}

//This class is used to instanciate the Encoder. It will automatically decide which ffmpeg library to use,
//the native or embedded one, according to the SO.
class FFMPEGLocatorImp extends FFMPEGLocator {
	@Override
	protected String getFFMPEGExecutablePath() {
		String osname = System.getProperty("os.name");
		String path = null;
		if (osname.toLowerCase().contains("mac")) {
			path = new File("ffmpeg_mac").getAbsolutePath();
		}
		else if (osname.toLowerCase().contains("linux")) {
			path = new File("ffmpeg_linux").getAbsolutePath();
		}
		return path;
	}
}
