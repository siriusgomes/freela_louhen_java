package audio_converter;

import java.io.File;

import it.sauronsoftware.jave.AudioAttributes;
import it.sauronsoftware.jave.Encoder;
import it.sauronsoftware.jave.EncoderProgressListener;
import it.sauronsoftware.jave.EncodingAttributes;
import it.sauronsoftware.jave.FFMPEGLocator;
import it.sauronsoftware.jave.MultimediaInfo;

public class AudioConverter implements EncoderProgressListener {

	public AudioConverter(String sourceFile, String targetFile) {
		try {
			Encoder encoder = new Encoder(new FFMPEGLocatorImp());

			File source = new File(sourceFile);
			File target = new File(targetFile);

			// Setting audio attributes
			AudioAttributes audio = new AudioAttributes();
			audio.setCodec("ac3");
			audio.setBitRate(encoder.getInfo(source).getAudio().getBitRate());
			audio.setChannels(encoder.getInfo(source).getAudio().getChannels());
			audio.setSamplingRate(encoder.getInfo(source).getAudio().getSamplingRate());
			EncodingAttributes attrs = new EncodingAttributes();
			attrs.setFormat("ac3");
			attrs.setAudioAttributes(audio);
			
			// Encoding
			encoder.encode(source, target, attrs, this);
		} catch (Exception e) {
			System.err.println("Error: " + e.getMessage());
			e.printStackTrace();
		}	
	}
		
	
	public static void main(String[] args) {
//		String[] args2 = {"ACDC_Back_in_Black.mp3", "Test.ac3"};
//		args = args2;
		if (args.length != 2) {
			System.out.println("Usage: java -jar AudioConverter.jar <mp3 input file> <ac3 file output>");
		}
		else {
			new AudioConverter(args[0], args[1]);
		}
	}

	@Override
	public void message(String arg0) {
	}

	@Override
	public void progress(int arg0) {
		System.out.println("Conversion " + (arg0 / 10.0) + "% done");
	}

	@Override
	public void sourceInfo(MultimediaInfo arg0) {
	}

}

class FFMPEGLocatorImp extends FFMPEGLocator {
	@Override
	protected String getFFMPEGExecutablePath() {
		String osname = System.getProperty("os.name");
		if (osname.toLowerCase().contains("mac")) {
			return new File("ffmpeg_mac").getAbsolutePath();
		}
		else if (osname.toLowerCase().contains("linux")) {
			return new File("ffmpeg_linux").getAbsolutePath();
		}
		else {
			return new File("ffmpeg_win").getAbsolutePath();
		}
		
	}
}
